<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', [\App\Http\Controllers\LandingpageController::class, 'index']);

Route::get('/login', [\App\Http\Controllers\AuthController::class, 'index'])->name('login');
Route::post('/cek-login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login-cek');
Route::get('/logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout');

Route::group(['middleware' => 'auth'], function () {
   Route::get('/home', [\App\Http\Controllers\DashboardController::class, 'index'])->name('home');

   Route::resource('user', \App\Http\Controllers\UserController::class);
   Route::resource('post', \App\Http\Controllers\PostController::class);
});
