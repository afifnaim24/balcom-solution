<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'tittle'        => 'Postingan',
                'description'   => '<p>Ini adalah postingan</p><p>Ini adalah paragraf kedua</p>',
                'slug'          => 'postingan',
                'is_display'    => 1
            ],
        ];

        foreach ($data as $key => $value) {
            Post::create($value);
        }
    }
}
