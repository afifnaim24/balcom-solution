<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'              => 'Admin',
                'email'             => 'admin@balcom-solution.com',
                'password'          => Hash::make('123456789'),
                'is_active'         => 1,
                'email_verified_at' => Carbon::now()
            ],
        ];

        foreach ($data as $key => $value) {
            User::create($value);
        }
    }
}
