@extends('layout.skeleton')

@section('app')
    <div class="main-wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            @include('partials.topnav')
        </nav>
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            @include('partials.sidebar')
        </aside>

        <!-- Main Content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

    </div>
    <footer class="main-footer">
        @include('partials.footer')
    </footer>

    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>

    </div>
@endsection
