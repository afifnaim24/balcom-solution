@extends('layout.skeleton')

{{--<link rel="icon" href="{{ asset('others/img/favicon.png') }}" type="image/x-icon">--}}

<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="#">
            <b>BALCOM</b>
            <p>SOLUTION
            </p>
        </a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">

            @include('partials.message')

            <form method="POST" action="{{ route('login-cek') }}" class="needs-validation" novalidate="">
                @csrf
                <div class="input-group mb-3">
                    <input type="email" class="form-control" value="{{ old('email') }}" name="email" id="email" placeholder="E-Mail">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
@include('partials.footer')

