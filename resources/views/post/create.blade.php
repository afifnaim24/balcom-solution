@extends('layout.app')

@push('css')
    <style>
        #image-preview {
            display: flex;
            flex-wrap: wrap;
        }

        .img-thumbnail {
            margin: 20px;
            border: 2px solid #ccc;
            border-radius: 5px;
            padding: 5px;
        }
    </style>

@endpush

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{$tittle}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">{{$tittle}}</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- Main row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Fom {{$tittle}}</h5>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="tittle">Judul Artikel</label>
                                        <input type="text" name="tittle" value="{{ old('tittle') }}" class="form-control @error('tittle') is-invalid @enderror" id="tittle" placeholder="Isi Judul">
                                        @error('tittle')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="description">Deskripsi</label>
                                        <textarea id="summernote" name="description">{{ old('description') }}</textarea>
                                        @error('description')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="description">Gambar</label>
                                        <input type="file" name="images" id="images" multiple accept="image/jpeg, image/png, image/jpg, image/gif">
                                        <div id="image-preview" class="mt-2"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <a href="{{ route('post.index') }}" class="btn btn-danger"><i class="fas fa-chevron-left"></i> Kembali</a>
                                    <button class="btn btn-success"><i class="fas fa-save"></i> Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
@endsection

@push('js')
    <script>
        document.getElementById('images').addEventListener('change', function() {
            var imagePreview = document.getElementById('image-preview');
            imagePreview.innerHTML = ''; // Bersihkan tampilan preview sebelumnya

            for (var i = 0; i < this.files.length; i++) {
                var file = this.files[i];
                if (file.type.match('image.*')) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        var img = document.createElement('img');
                        img.className = 'img-thumbnail m-2'; // Ganti sesuai kebutuhan
                        img.style.maxWidth = '150px'; // Ganti sesuai kebutuhan
                        img.src = e.target.result;
                        imagePreview.appendChild(img);
                    };

                    reader.readAsDataURL(file);
                }
            }
        });
    </script>
@endpush
