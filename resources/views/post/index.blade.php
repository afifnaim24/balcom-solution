@extends('layout.app')

@section('content')

@push('css')
    <!-- datatable -->
@endpush

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ $tittle }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ $tittle }}</a></li>
                        <li class="breadcrumb-item active">Index</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('post.create') }}" class="btn btn-success"><i class="fas fa-plus"></i> Tambah Data : {{$tittle}}</a>
                        </div>
                        <div class="card-body">
                            @include('partials.message')
                            <table class="table table-striped table-bordered" id="dt_table">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Slug</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($posts as $post)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ $post->tittle }}</td>
                                        <td>{{ $post->slug }}</td>
                                        <td>{{ $post->status }}</td>
                                        <td>
                                            <form action="{{ route('post.destroy', $post) }}" method="POST">
                                                <a href="{{ route('post.show', $post) }}" class="btn btn-success btn-sm">Show</a>
                                                <a href="{{ route('post.edit', $post) }}" class="btn btn-warning btn-sm">Edit</a>
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
@endsection

@push('js')
    <script>
        $(document).ready(function(){
            var dt = $('#dt_table').DataTable({
                autoWidth: false,
                searching: true,
                responsive: true,
            });

            dt.on('order.dt search.dt', function () {
                let i = 1;

                dt.cells(null, 0, { search: 'applied', order: 'applied' }).every(function (cell) {
                    this.data(i++);
                });
            }).draw();
        })
    </script>
@endpush
