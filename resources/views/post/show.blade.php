@extends('layout.app')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">User</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item active">Show</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- Main row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Fom User</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="name">Nama</label>
                                    <input type="text" name="name" value="{{ $user->name }}" class="form-control @error('name') is-invalid @enderror" id="name" readonly>
                                    @error('name')
                                    <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" value="{{ $user->email }}" class="form-control @error('email') is-invalid @enderror" id="email" readonly>
                                    @error('email')
                                    <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" value="{{ $user->username }}" class="form-control @error('username') is-invalid @enderror" id="username" readonly>
                                    @error('username')
                                    <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <select class="form-control" name="role" id="role" disabled>
                                        <option value="">Pilih Role</option>
                                        <option value="admin" @if ($user->role == "admin") {{ 'selected' }} @endif>Admin</option>
                                        <option value="user" @if ($user->role == "user") {{ 'selected' }} @endif>User</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="id_bidang">Bidang</label>
                                    <select class="form-control" name="id_bidang" id="id_bidang" disabled>
                                        <option value="">Pilih Bidang</option>
                                        @foreach(\App\Models\Bidang::all() as $data)
                                            <option value="{{ $data->id }}" @if ($user->id_bidang == $data->id) {{ 'selected' }} @endif>{{ $data->nama_bidang }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <a href="{{ route('user.index') }}" class="btn btn-danger"><i class="fas fa-chevron-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
@endsection
