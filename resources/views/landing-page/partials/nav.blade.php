<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="#page-top">
            <img class="img-fluid" src="{{ asset('images/main-logo.png') }}" alt="Logo" style="max-height: 40px;" id="mainLogo">
        </a>
{{--        <a class="navbar-brand" href="#page-top">Start Bootstrap</a>--}}
        <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ms-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                <li class="nav-item"><a class="nav-link" href="#services">Services</a></li>
                <li class="nav-item"><a class="nav-link" href="#portfolio">Portfolio</a></li>
                <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
@push('js')
    <script>
        window.addEventListener('scroll', function() {
            var logo = document.getElementById('mainLogo');
            var scrolled = window.scrollY;
            var threshold = 100; // Change this value according to when you want the change to occur

            // Check if the page has been scrolled beyond the threshold
            if (scrolled > threshold) {
                // Change the logo to the new one
                logo.src = "{{ asset('images/secondary-logo.png') }}";
            } else {
                // Revert back to the original logo
                logo.src = "{{ asset('images/main-logo.png') }}";
            }
        });
    </script>
@endpush
