@extends('landing-page.layout.skeleton')

@section('landing-page')

    @include('landing-page.partials.nav')

    @yield('page')

    @include('landing-page.partials.footer')

@endsection
