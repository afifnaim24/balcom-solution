@extends('layout.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">User</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Fom User</h5>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('user.update', $user->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name">Nama</label>
                                            <input type="text" name="name" value="{{ $user->name }}" class="form-control @error('name') is-invalid @enderror" id="name">
                                            @error('name')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" name="email" value="{{ $user->email }}" class="form-control @error('email') is-invalid @enderror" id="email">
                                            @error('email')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input type="text" name="username" value="{{ $user->username }}" class="form-control @error('username') is-invalid @enderror" id="username">
                                            @error('username')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="role">Role</label>
                                            <select class="form-control" name="role" id="role">
                                                <option value="">Pilih Role</option>
                                                <option value="admin" @if ($user->role == "admin") {{ 'selected' }} @endif>Admin</option>
                                                <option value="user" @if ($user->role == "user") {{ 'selected' }} @endif>User</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="id_bidang">Bidang</label>
                                            <select class="form-control" name="id_bidang" id="id_bidang">
                                                <option value="">Pilih Bidang</option>
                                                @foreach($bidang as $data)
                                                    <option value="{{ $data->id }}" @if ($user->id_bidang == $data->id) {{ 'selected' }} @endif>{{ $data->nama_bidang }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">
                                            @error('password')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="password">Konfirmasi Password</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" autocomplete="current-password">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="{{ route('user.index') }}" class="btn btn-danger"><i class="fas fa-chevron-left"></i> Kembali</a>
                                        <button class="btn btn-success"><i class="fas fa-save"></i> Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('js')
<script src="{{ asset('adminlte') }}/plugins/select2/js/select2.full.min.js"></script>

<script>
    $(document).ready(function(){
        $('#role').select2({
            theme: 'bootstrap4',
            placeholder: 'Pilih Role'
        })
        $('#id_bidang').select2({
            theme: 'bootstrap4',
            placeholder: 'Pilih Bidang'
        })
    })
</script>
@endpush
