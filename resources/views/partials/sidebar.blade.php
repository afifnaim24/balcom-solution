@php
    $role = Auth::user()->role;
@endphp

    <!-- Brand Logo -->
<a href="{{ route('home') }}" class="brand-link">
{{--    <img src="{{ asset('others/img/favicon.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">--}}
    <span class="brand-text font-weight-light">BALCOM SOLUTION</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
     <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('adminlte/dist/img/AdminLTELogo.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="" class="d-block">{{ auth()->user()->name }}</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
{{--    <nav class="mt-2">--}}
{{--        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">--}}
{{--            <!-- Add icons to the links using the .nav-icon class--}}
{{--                 with font-awesome or any other icon font library -->--}}
{{--            <li class="nav-item bg-amber">--}}
{{--                <a href="#" class="nav-link">--}}
{{--                    <i class="nav-icon fas fa-calendar"></i>--}}
{{--                    <p class="text-danger">Sisa Waktu Input</p>--}}
{{--                    <br/>--}}
{{--                    <b id="sisa_waktu" style="font-size: 12pt !important">0 Hari 0 Jam 0 Menit 0 Detik</b>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="nav-header">DASHBOARD</li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="{{ route('home') }}" class="nav-link {{ Route::is('home') ? 'active' : '' }}">--}}
{{--                    <i class="nav-icon fas fa-tachometer-alt"></i>--}}
{{--                    <p>--}}
{{--                        Dashboard--}}
{{--                    </p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="nav-header">MAIN MENU LKPJ</li>--}}
{{--            <li class="nav-item">--}}
{{--            <li class="nav-item has-treeview {{ Route::is('pks.index') || Route::is('indikator.*') || Route::is('anggaran-realisasi.*') ? 'menu-is-opening menu-open' : '' }}">--}}
{{--                <a href="#" class="nav-link">--}}
{{--                    <i class="nav-icon fas fa-file-alt"></i>--}}
{{--                    <p>--}}
{{--                        LKPJ--}}
{{--                        <i class="fas fa-angle-left right"></i>--}}
{{--                    </p>--}}
{{--                </a>--}}
{{--                <ul class="nav nav-treeview">--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('pks.index') }}" class="nav-link {{ Route::is('pks.index') ? 'active' : '' }}">--}}
{{--                            <i class="far fa-circle nav-icon"></i>--}}
{{--                            <p>Prog/Keg/SubKeg</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('indikator.index') }}" class="nav-link {{ Route::is('indikator.*') ? 'active' : '' }}">--}}
{{--                            <i class="far fa-circle nav-icon"></i>--}}
{{--                            <p>Capaian Indikator</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('anggaran-realisasi.index') }}" class="nav-link {{ Route::is('anggaran-realisasi.*') ? 'active' : '' }}">--}}
{{--                            <i class="far fa-circle nav-icon"></i>--}}
{{--                            <p>Capaian Anggaran & <br/> Realisasi</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li class="nav-item has-treeview">--}}
{{--                <a href="#" class="nav-link">--}}
{{--                    <i class="nav-icon fas fa-print"></i>--}}
{{--                    <p>--}}
{{--                        Cetak Prog/Keg/SubKeg--}}
{{--                    </p>--}}
{{--                    <i class="fas fa-angle-left right"></i>--}}
{{--                </a>--}}
{{--                <ul class="nav nav-treeview">--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('cetak.index', ['export' => 'pdf']) }}" target="_blank" class="nav-link">--}}
{{--                            <i class="far fa-circle nav-icon"></i>--}}
{{--                            <p>PDF</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('cetak.index', ['export' => 'excel']) }}" target="_blank" class="nav-link">--}}
{{--                            <i class="far fa-circle nav-icon"></i>--}}
{{--                            <p>EXCEL</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            @if ($role == 'admin')--}}
{{--                <li class="nav-header">ADMINISTRATOR MENU</li>--}}
{{--                <li class="nav-item has-treeview {{ Route::is('bidang.*') || Route::is('sasaran.*') || Route::is('satuan.*') || Route::is('tahun.*') ? 'menu-is-opening menu-open' : '' }}">--}}
{{--                    <a href="#" class="nav-link">--}}
{{--                        <i class="nav-icon fas fa-database"></i>--}}
{{--                        <p>--}}
{{--                            Master Data--}}
{{--                            <i class="fas fa-angle-left right"></i>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <ul class="nav nav-treeview">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('bidang.index') }}" class="nav-link {{ Route::is('bidang.*') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Bidang</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('sasaran.index') }}" class="nav-link {{ Route::is('sasaran.*') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Sasaran</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('satuan.index') }}" class="nav-link {{ Route::is('satuan.*') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Satuan</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('tahun.index') }}" class="nav-link {{ Route::is('tahun.*') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Tahun</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{{ route('user.index') }}" class="nav-link {{ Route::is('user.*') ? 'active' : '' }}">--}}
{{--                        <i class="nav-icon fas fa-user"></i>--}}
{{--                        <p>--}}
{{--                            Master User--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{{ route('jadwal.index') }}" class="nav-link {{ Route::is('jadwal.*') ? 'active' : '' }}">--}}
{{--                        <i class="nav-icon fas fa-cog"></i>--}}
{{--                        <p>--}}
{{--                            Setting Jadwal--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--        </ul>--}}
{{--    </nav>--}}
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
