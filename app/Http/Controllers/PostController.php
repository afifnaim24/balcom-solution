<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        $this->tittle = 'Post';
    }

    public function index(Request $request)
    {
        $posts = Post::all();
        $tittle = $this->tittle;

        return view('post/index', compact('posts', 'tittle'));
    }

    public function create()
    {
        $tittle = $this->tittle;

        return view('post/create', compact('tittle'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // Tambahkan aturan validasi untuk file gambar di sini sesuai kebutuhan Anda
            'images' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($validator->fails()) {dd($validator);
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            DB::beginTransaction();

            $request->merge([
               'slug' => $this->generateSlug($request->tittle)
            ]);

            $post = Post::create($request->all());
//            dd($request);
            if ($request->hasFile('images')) {
                    insert_picture($request->images, $post);
            }
//            dd($post->picture);
            DB::commit();
            return redirect()->route('post.index')->with('Data berhasil ditambah!');
        } catch (\Exception $exception) {
//            DB::rollBack();
            dd($exception);
            return redirect()->route('post.index')->with($exception->getMessage());
        }
    }

    public function show(Post $post)
    {dd($post->picture->url);
        $tittle = $this->tittle;

        return view('post/show', compact('post', 'tittle'));
    }

    private function generateSlug($title)
    {
        $slug = strtolower(str_replace(' ', '-', $title));

        $slug = preg_replace('/[^a-z0-9\-]/', '', $slug);

        $slug = preg_replace('/-+/', '-', $slug);

        if (empty($slug)) {
            $slug = 'untitled';
        }

        return $slug;
    }
}
