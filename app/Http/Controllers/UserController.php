<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::all();

        return view('user.index', compact('users'));
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name"      => 'required|max:255',
            "email"     => 'required|unique:users,email',
            "password"  => 'required|min:4',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.index')
                ->withErrors($validator->errors())
                ->withInput();
        }

        $data = array(
            "name"              => $request->name,
            "email"             => $request->email,
            "password"          => $request->password,
            "is_active"         => 1,
            "email_verified_at" => Carbon::now()
        );

        return redirect()->route('user.index');
    }
}
