<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth/index');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->only('email', 'password'), [
            "email"      => 'required',
            "password"   => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('login-cek')
                ->withErrors($validator->errors())
                ->withInput();
        }

        if (auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))) {
            return redirect('home');
        } else {
            return redirect()->route('login')->withInput()->with('error', 'E-Mail/Password Salah.');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('login');
    }
}
