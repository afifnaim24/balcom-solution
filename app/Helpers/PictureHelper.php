<?php

if (!function_exists('insert_picture')) {
    function insert_picture($picture, $model, $caption = null, $base64 = false, $type = null) {
        $service = new \App\Services\PictureService();
        $folder = class_basename($model);
        $location = 'images/'.$folder;

        if($base64 == true){
            $pos  = strpos($picture, ';');
            $type = explode(':', substr($picture, 0, $pos))[1];

            $extension = explode('/', $type)[1];
            if($extension=='jpeg')$extension='jpg';

            $name = uniqid();
            $path = $location.'/'.$name.'.'.$extension;

            $service->save_base64_image($picture, storage_path('app/public/'.$path));

            $picture = $model->picture()->create([
                'path' => $location,
                'file_name' => $name .'.'. $extension,
                'caption' => $caption,
                'type' => $type
            ]);

            return $picture;

        }

        return $service->upload($picture, $model, $location, $caption, $type);
    }
}


if (!function_exists('insert_pictures')) {
    function insert_pictures($pictures, $model, $caption = null) {
        $uploaded = collect();

        foreach ($pictures as $picture) {
            $pic = insert_picture($picture, $model, $caption);
            $uploaded->push($pic);
        }

        return $uploaded;
    }
}


if (!function_exists('get_picture_html')) {
    function get_picture_html($url, $class=null, $height = null) {
        return '<img src="'.asset('storage/'.$url).'"
                     onerror="this.onerror=null;this.src=\' '.asset('images/404.jpg').' \';"
                     class="'.$class.'"
                     width='.$height.'>';
    }
}
