<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Picture extends Model
{

    protected $fillable = [
        'pictureable_id',
        'pictureable_type',
        'path',
        'file_name',
        'caption',
        'note',
    ];

    public function pictureable() : MorphTo
    {
        return $this->morphTo();
    }

    public function getUrlAttribute()
    {
        if (is_null($this->path)) {
            return asset('images/logo_watermark.jpg');
        }

        return asset($this->path.'/'.$this->file_name);
    }
}
