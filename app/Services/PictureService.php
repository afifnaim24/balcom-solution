<?php

namespace App\Services;

use Intervention\Image\Facades\Image;

class PictureService
{

    public function upload($picture, $model, $location, $caption, $type)
    {
        if (is_string($picture)) {
            // Jika $picture adalah base64, lakukan penanganan khusus
            $pos  = strpos($picture, ';');
            $type = explode(':', substr($picture, 0, $pos))[1];

            $extension = explode('/', $type)[1];
            if ($extension == 'jpeg') $extension = 'jpg';

            $name = uniqid();
            $imageName = $name .'.'. $extension;

            $path = $location.'/'.$imageName;

            $this->save_base64_image($picture, storage_path('app/public/'.$path));

            $picture = $model->picture()->create([
                'path' => $location,
                'file_name' => $imageName,
                'caption' => $caption,
                'type' => $type,
            ]);
        } else {
            // Jika $picture adalah objek UploadedFile, lanjutkan seperti biasa
            $imageName = $picture->hashName();

            $img = Image::make($picture);

            $img->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
            });

            $resource = $img->stream()->detach();

            \Illuminate\Support\Facades\Storage::disk('public')->put($location.'/'.$imageName, $resource, 'public');

            $picture = $model->picture()->create([
                'path' => $location,
                'file_name' => $imageName,
                'caption' => $caption,
                'type' => $type,
            ]);
        }

        return $picture;
    }

}
